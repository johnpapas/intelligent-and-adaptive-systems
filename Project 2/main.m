%Intelligent and Adaptive Systems Project 2
%Author Papas Ioannis 9218
%%
%First question/Unstable

x0=[0.7 0.06];  %Change the initial conditions from here
tspan=0:1:5000;
[~,x]=ode45(@(t,y) odefun(t,y), tspan, x0);
figure(1)
clf
plot(x(:,1))
hold on
plot(x(:,2))
title(sprintf("p and � diagram based on time for initial conditions [%.3f %.3f]", x0(1), x0(2)));
xlabel("Time")
ylabel("Angle and angular velocity")
legend("�", "p")
figure(2)
clf
plot(x(:,1),x(:,2))
title(sprintf("p-� diagram for initial conditions [%.3f %.3f]", x0(1), x0(2)));
xlabel("Angle")
ylabel("Angular velocity")

%%
%Fifth question
k=50; %When we want matrix Q to be k*I where I the identity matrix
x0=[0 0 0 0 0 0 0 0 0 0];
tspan=0:1:140;
[t,x]=ode23(@(t,y) odefun1(t,y,k), tspan, x0);

%Input function plotting

r=0.1745*sin(t);

% oldparam = sympref('HeavisideAtOrigin',1); %Changes the value of the heaviside function when x=0->f(x)=1 instead of f(x)=1/2
% r=(5*pi/180)*(heaviside(t-1)-heaviside(t-10))+(-5*pi/180)*(heaviside(t-22)-...
% heaviside(t-35))+(10*pi/180)*(heaviside(t-45)-heaviside(t-55))+(-10*pi/180)*...
% (heaviside(t-65)-heaviside(t-75))+(5*pi/180)*(heaviside(t-90)-heaviside(t-98))+...
% (-5*pi/180)*(heaviside(t-110)-heaviside(t-120));

figure(3)
clf
plot(t,r)
title("Plot of the input function")
xlabel("Time(s)")
ylabel("Input measured in rad")

%Error plotting
figure(4)
clf
plot(t,x(:,7)-x(:,9),t,x(:,8)-x(:,10))
title(sprintf("Plot of regulation error for Q=%d*I", k))
legend("�-�ref", "p-pref")
xlabel("Time (s)")
ylabel({"Value of error measured with rad for angle";"and rad/s for angular velocity"})

%Angle plotting
figure(5)
clf
plot(t,x(:,7),t,x(:,9))
title(sprintf("Plot of plant and reference model angle for Q=%d*I", k))
legend("�", "�ref")
xlabel("Time (s)")
ylabel("Value of angle measured with rad")

%Angular velocity plotting
figure(6)
clf
plot(t,x(:,8),t,x(:,10))
title(sprintf("Plot of plant and reference model angular velocity for Q=%d*I", k))
legend("p", "pref")
xlabel("Time (s)")
ylabel("Value of angular velocity measured with rad/s")

%Adaptive gains plotting
figure(7)
clf
plot(t,x(:,1),t,x(:,2))
title(sprintf("Plot of the K=[k1 k2] adaptive gains for Q=%d*I", k))
legend("k1", "k2")
xlabel("Time (s)")
ylabel("Value of the adaptive gains")
figure(8)
clf
plot(t,x(:,3))
title(sprintf("Plot of the L=l1 adaptive gain for Q=%d*I", k))
xlabel("Time (s)")
ylabel("Value of the adaptive gain")
figure(9)
clf
plot(t,x(:,4),t,x(:,5),t,x(:,6))
title(sprintf("Plot of the M=[�1 �2 �3] adaptive gains for Q=%d*I", k))
legend("�1", "�2", "�3")
xlabel("Time (s)")
ylabel("Value of the adaptive gains")

%Controller plotting
u=-x(1)*x(7)-x(2)*x(8)+x(3)*r-x(4)*abs(x(7))*x(8)-x(5)*abs(x(8))*x(8)-x(6)*(x(7)^3);
figure(10)
clf
plot(t,u)
title("Plot of the control law")
xlabel("Time(s)")
ylabel("Value of the control law")










