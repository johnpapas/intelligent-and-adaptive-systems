%Intelligent and Adaptive Systems Project 2
%Author Papas Ioannis 9218

function dxdt=odefun(t,x)

v=0;
dxdt=zeros(2,1);
dxdt(1)=x(2);
dxdt(2)=-0.018*x(1)+0.015*x(2)+(-0.062*abs(x(1))+0.009*abs(x(2)))*x(2)+0.021*x(1)^3+0.75*v;


end