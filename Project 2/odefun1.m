%Intelligent and Adaptive Systems Project 2
%Author Papas Ioannis 9218

function dxdt=odefun1(t,x,k)
p2=k/2;
p3=k*10/14;
theta=[-0.018 0.015 -0.062 0.009 0.021 0.75];

% oldparam = sympref('HeavisideAtOrigin',1); %Changes the value of the heaviside function when x=0->f(x)=1 instead of f(x)=1/2
% r=(5*pi/180)*(heaviside(t-1)-heaviside(t-10))+(-5*pi/180)*(heaviside(t-22)-...
% heaviside(t-35))+(10*pi/180)*(heaviside(t-45)-heaviside(t-55))+(-10*pi/180)*...
% (heaviside(t-65)-heaviside(t-75))+(5*pi/180)*(heaviside(t-90)-heaviside(t-98))+...
% (-5*pi/180)*(heaviside(t-110)-heaviside(t-120));

r=0.1745*sin(t);
    
dxdt(1)=(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*x(7);    %k1
dxdt(2)=(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*x(8);     %k2
dxdt(3)=-(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*r;     %l1
dxdt(4)=(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*abs(x(7))*x(8);     %m1
dxdt(5)=(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*abs(x(8))*x(8);     %m2
dxdt(6)=(p2*(x(7)-x(9))+p3*(x(8)-x(10)))*(x(7)^3);     %m3
dxdt(7)=x(8);    %f
dxdt(8)=(theta(1)-theta(6)*x(1))*x(7)+(theta(2)-theta(6)*x(2))*x(8)+theta(6)*...
x(3)*r+theta(6)*(((theta(3)/theta(6))-x(4))*abs(x(7))*x(8)+...
((theta(4)/theta(6))-x(5))*abs(x(8))*x(8)+((theta(5)/theta(6))-x(6))*(x(7)^3));%p
dxdt(9)=x(10);   %fref
dxdt(10)=-x(9)-1.4*x(10)+r;   %pref
dxdt=dxdt';


end